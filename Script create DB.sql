﻿-- CREATE TABLES
CREATE TABLE USERS 
(
	id varchar(50) primary key,
	pass varchar(50) not null,
	fullname varchar(50) not null,
	birthday date not null,
	sex varchar(6) not null,
	phone varchar(15) not null,
	address varchar(200) not null,
	captcha_id serial not null,  -- autonumber increment
	located varchar(30) not null,  -- country
	createdate timestamp without time zone not null -- get datetime now
);

CREATE TABLE CAPTCHA  -- check you are bot 
(
        id serial primary key, -- autonumber increment
        ip_address varchar(45) not null,
        word varchar(20) not null -- word captcha       
);

CREATE TABLE HISTORYLOGIN  -- check history users login
(
        id serial primary key, -- autonumber increment
        user_id varchar(50) not null,
        logintime timestamp without time zone not null
);

CREATE TABLE CHANGEPASSWORD  -- history users change password
(
        id serial primary key, -- autonumber increment
	user_id varchar(50) not null,	-- not change
	oldpass varchar(50) not null,
	newpass varchar(50) not null,
        lastupdate timestamp without time zone not null -- get datetime now
);

CREATE TABLE CHANGEINFOUSER -- history users change infomation
(
        id serial primary key, -- autonumber increment
	user_id varchar(50) not null,   -- not change
	fullname varchar(50) not null,
	birthday date not null,
	sex varchar(6) not null,
	phone varchar(15) not null,
        address varchar(200) not null,
        located varchar(30) not null,  -- country
        lastupdate timestamp without time zone not null -- get datetime now
);

--------------------------------------
-- INSERT TABLES
-- Insert parallel Table USERS and table CAPTCHA  			
INSERT INTO USERS(id,pass,fullname,birthday,sex,phone,address,located,createdate) VALUES('myuser1','********','Tinh Truong','1989-04-23','Male','+8492786532','25 CMT8, P.7, Q.TB, HCM','Vietnam','2016-12-12 21:43:46.865842');
INSERT INTO CAPTCHA(ip_address,word) VALUES ('192.168.2.4','R54Fcar');

INSERT INTO USERS(id,pass,fullname,birthday,sex,phone,address,located,createdate) VALUES('myuser2','********','Hao Do','1989-09-08','Male','+8492388599','95 Bach Dang, P.7, Q.TB, HCM','Vietnam','2016-12-18 12:27:34.865842');
INSERT INTO CAPTCHA(ip_address,word) VALUES ('192.168.8.241','F845Rasd');

INSERT INTO USERS(id,pass,fullname,birthday,sex,phone,address,located,createdate) VALUES('myuser3','********','Hong Tran','1987-02-03','Female','+8492345459','97 Bach Dang, P.7, Q.TB, HCM','Vietnam','2017-07-12 19:25:45.865842');
INSERT INTO CAPTCHA(ip_address,word) VALUES ('192.168.3.3','3Dtqa21');

INSERT INTO USERS(id,pass,fullname,birthday,sex,phone,address,located,createdate) VALUES('myuser4','********','Minh Dang','1988-03-18','Male','+8492325896','90 Bach Dang, P.2, Q.TB, HCM','Vietnam','2017-07-16 14:42:23.865842');
INSERT INTO CAPTCHA(ip_address,word) VALUES ('192.168.12.44','G34faDr');

INSERT INTO USERS(id,pass,fullname,birthday,sex,phone,address,located,createdate) VALUES('myuser5','********','Hang Le','1988-05-22','Male','+8492112226','94 Le Binh, P.3, Q.BT, HCM','Vietnam','2017-07-23 12:34:13.865842');
INSERT INTO CAPTCHA(ip_address,word) VALUES ('192.168.6.45','G34faDr');

INSERT INTO USERS(id,pass,fullname,birthday,sex,phone,address,located,createdate) VALUES('myuser6','********','Dinh Long','1989-05-01','Male','+8492342432','1206 CMT8, P.7, Q.TB, HCM','Vietnam',now());
INSERT INTO CAPTCHA(ip_address,word) VALUES ('192.168.4.7','Ar3F5H');

INSERT INTO USERS(id,pass,fullname,birthday,sex,phone,address,located,createdate) VALUES('myuser7','********','Chau Phan','1990-11-21','Female','+8496987542','75 Nguyen Hue, P.1, Q.1, HCM','Vietnam',now());
INSERT INTO CAPTCHA(ip_address,word) VALUES ('192.168.4.16','Oh3F4sg');


-- I can check system growing last year, last month, this month and this year
SELECT SUM(CASE WHEN date_part('year',createdate) = date_part('year',now()) - 1 THEN 1 ELSE 0 END) as LASTYEAR,  
	SUM(CASE WHEN date_part('year',createdate) = date_part('year',now()) and date_part('month',createdate) = date_part('month',now()) - 1 THEN 1 ELSE 0 END) as LASTMONTH,
	SUM(CASE WHEN date_part('year',createdate) = date_part('year',now()) and date_part('month',createdate) = date_part('month',now()) THEN 1 ELSE 0 END) as THISMONTH,
	SUM(CASE WHEN date_part('year',createdate) = date_part('year',now()) THEN 1 ELSE 0 END) as THISYEAR
FROM USERS;

/*
- If you want the databse to extend for future requirements you cant create more table or add column in tables
*/



















